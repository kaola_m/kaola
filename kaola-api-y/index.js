const express = require('express');
const app = express();


app.use(require('cors')());

const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended:true}));


const categoriesRoutes  = require('./routes/categoriesRoutes');

app.use(categoriesRoutes);


app.listen(8888,()=>{
  console.log("server is running at http://localhost:8888");
})