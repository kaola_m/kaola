const express = require('express');

const router = express.Router();

const config = require("../config");

const db = require("../db");

let api = config.api.str;


// 获取分类左侧以及分类
router.get(`${api}/categories_left`,(req,res)=>{
  // console.log(11);
  let sql = 'select * from categories_left';
  db.query(sql,(err,data)=>{
    // res.json(data);
    if(err){
      res.json({
        "code":0,
        "err":err
      })
    }else{
      res.json({
        "code":1,
        "data":data
      })
    }
  })
})
// 分类页面右侧三级导航
router.get(`${api}/categories_right`,(req,res)=>{
  let right_id = req.query.right_id;
  let sql = `select * from categories_right where right_id = ${right_id};`
  db.query(sql,(err,data)=>{
    if(err){
      res.json({
        "code":0,
        "err":err
      })
    }else{
      res.json({
        "code":1,
        "data":data
      })
    }
  })

//   db.query(sql,req.query.right_id,(err,result)=>{
//     let data = result[0];
//     window.console.log(data)
//     // for(let i = 0 ; i < data.length;i++){
//     //   data[i].children = [];
//     // }
//     // for(let i =0 ; i <= res[1].length-1;i++){
//     //   for(let i = 0; i<data.length;i++){
//     //     if(res[1][i].right_id == data[i].id){
//     //       data[i].children.push(res[1][i])
//     //       continue;
//     //     }
//     //   }
//     // }
//   })
})
router.get(`${api}/categories_sc_right`,(req,res)=>{
  let id = req.query.right_id;
  let sql = `select distinct cate_sc_name from categories_right where right_id = ${id}`
  db.query(sql,(err,data)=>{
    if(err){
      res.json({
        "code":0,
        "err":err
      })
    }else{
      res.json({
        "code":1,
        "data":data
      })
    }
  })
})


router.get(`${api}/catelist`,(req,res)=>{
  let cate_id = req.query.cate_id;
  console.log(cate_id);
  let sql = `select * from categories_items where cate_id = ${cate_id} `;
  db.query(sql,(err,data)=>{
    console.log(data);
    if(err){
      res.json({
        "code":0,
        "err":err
      })
    }else{
      res.json({
        "code":1,
        "data":data
      })
    }
  })
})


router.get(`${api}/cate_count`,(req,res)=>{
  let cate_id = req.query.cate_id;
  console.log(cate_id);
  let sql = `select * from categories_items where cate_id = ${cate_id} order by  items_count`;
  db.query(sql,(err,data)=>{
    console.log(data);
    if(err){
      res.json({
        "code":0,
        "err":err
      })
    }else{
      res.json({
        "code":1,
        "data":data
      })
    }
  })
})


router.get(`${api}/item_swiper`,(req,res)=>{
  let id = req.query.cate_item_id;
  // console.log(id);
  let sql = `select * from item_swiper where cate_item_id = ${id}`;
  db.query(sql,(err,data)=>{
    if(err){
      res.json({
        "code":0,
        "err":err
      })
    }else{
      res.json({
        "code":1,
        "data":data
      })
    }
  })
})

// 获取商品价格
router.get(`${api}/goods_price`,(req,res)=>{
  let id = req.query.id;
  // console.log(id);
  let sql = `select * from categories_items where id = ${id}`
  db.query(sql,(err,data)=>{
    if(err){
      res.json({
        "code":0,
        "err":err
      })
    }else{
      res.json({
        "code":1,
        "data":data
      })
    }
  })
})


router.get(`${api}/goods_desc`,(req,res) =>{
  let id = req.query.goods_id;
  let sql = `select * from item_desc where goods_id = ${id}`;
  db.query(sql,(err,data)=>{
    if(err){
      res.json({
        "code":0,
        "err":err
      })
    }else{
      res.json({
        "code":1,
        "data":data
      })
    }
  })
})


module.exports = router;